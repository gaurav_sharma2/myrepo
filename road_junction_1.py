#!/usr/bin/env python
# coding: utf-8

# In[33]:


import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
import random
import psycopg2
import re
# import tempfile
# from sqlalchemy import create_engine


# In[43]:


connection = psycopg2.connect(user="postgres",
                                  password="Jaya#2707",
                                  host="ikshan.cknt8xlkk69r.us-east-1.rds.amazonaws.com",
                                  port="5432",
                                  database="postgres")
# with tempfile.TemporaryFile() as tmpfile:
#         copy_sql = "COPY ({query}) TO STDOUT WITH CSV {head}".format(
#            query="select * from map_data_road_junction WHERE grid_zoom9 IS NOT NULL ORDER BY cast(grid_zoom9 as int) ASC",
#             head="HEADER")
#         conn = cnx.raw_connection()
#         cur = conn.cursor()
#         #cur = connection.cursor()
#         cur.copy_expert(copy_sql, tmpfile)
#         tmpfile.seek(0)
#         df = pd.read_csv(tmpfile)
cursor = connection.cursor()

df = pd.read_sql_query("select * from map_data_road_junction WHERE grid_zoom9 IS NOT NULL ORDER BY cast(grid_zoom9 as int) ASC",con=connection)


# In[45]:


df.grid_zoom9


# In[48]:


#367309-531701
for i in range(57934,308105):
      xa=[]
      ya=[]
      z=df['grid_zoom9'][i]
      #print(i)
      #print("grid id:",df['grid_zoom9'][i])
      l1=df['polygon'][i]
      l2=re.findall('([0-9]*\.[0-9]*)', l1)
      #print("l1:",l1)
      #print("l2:",l2)
      x = float(l2[0])
      y = float(l2[1])
      #print(type(x))
      #print(y)
      centroid_x = float(df['centroid_x'][i])
      centroid_y = float(df['centroid_y'][i])
      name = df['name'][i]
      df['x_map'][i] = x - centroid_x
      df['y_map'][i] = y - centroid_y

      df['x_map1'][i] = df['x_map'][i]*200
      df['y_map1'][i] = df['y_map'][i]*200
      z=df['y_map1'][i]

      #random_index = random.randint(0,len(colours)-1)
      #c1 = colours[random_index]
      x_trans = 0
      y_trans = 0
      z_trans = 0
      y=0.04
      scale = 1
      #print(i)
    #Difference(
      #print(df['id'][i])
      if df['type'][i] == 'roundabout':
          df['x0_style'][i]='Torus('+str(0.1*scale)+','+str(0.007*scale)+',\'rotate\',[0,0,0],Texture(','Pigment(','\'checker\',\'color\',','[1,1,1],\'color\',','[0,0,0]))'
      if name is not None :
          x1 = "\""+str(name)+"\""
          #print(x1)
          print(i)
          s1 = "Box( ["+str(x-0.03)+","+str(0.02)+","+str(z+0.001)+"],["+str(x+0.03)+","+str(0.04)+","+str(z+0.002)+"],Finish( \'diffuse\', 0.9,\'metallic\',1),Pigment(\'color\', [1\255, 14\255, 133\255]))"
          s2 = "Text( \'ttf\', \'\"timrom.ttf\"\',"+x1+","+"0.01, 0,\'scale\', ["+str(0.01*scale)+","+str(0.01*scale)+",0.01],\'translate\',["+str(x-0.015)+","+str(0.027)+","+str(z-0.0001)+"],"+"\'rotate\'"+"["+str(0)+","+str(0)+","+str(0)+"]," #+"Pigment(\'color\', [1,1,1]))"
          s3 = "Box( ["+str((x-0.029)*scale)+","+str(0.021)+","+str(z+0.0009)+"],["+str((x+0.029)*scale)+","+"0.022,"+str(z+0.001)+"],Finish( \'diffuse\', 0.9,\'metallic\',1),Pigment(\'color\', [1,1,1]))"
          s4 = "Box( ["+str((x-0.029)*scale)+",0.038,"+str(z+0.0009)+"],["+str((x+0.029)*scale)+",0.039,"+str(z+0.001)+"],Finish( \'diffuse\', 0.9,\'metallic\',1),Pigment(\'color\', [1,1,1]))"
          s5 = "Box( [("+str((x-0.029)*scale)+","+"0.021,"+str(z+0.0009)+"],["+str((x-0.028)*scale)+",0.039,"+str(z+0.001)+"],Finish( \'diffuse\', 0.9,\'metallic\',1),Pigment(\'color\', [1,1,1])))" 
          s6 = "Box( [("+str((x+0.028)*scale)+",0.021,"+str(z+0.0009)+"],["+str((x+0.029)*scale)+",0.039,"+str(z+0.001)+"],Finish( \'diffuse\', 0.9,\'metallic\',1),Pigment(\'color\', [1,1,1])))" 
          s7 = "Cylinder(("+str(x+0)+",0,"+str(z+0.003)+"), ("+str(x+0)+","+str(0.03)+","+str(z+0.004)+"), 0.002,Finish(\'ambient\', 0.1, \'diffuse\', 0.6,\'metallic\',1),Pigment(\'color\', [0.95,0.87,0.85]))"
          df['x1_style'][i] = s1
          df['x2_style'][i] = s2
          df['x3_style'][i] = s3
          df['x4_style'][i] = s4
          df['x5_style'][i] = s5
          df['x6_style'][i] = s6
          df['x7_style'][i] = s7
          df['style'][i]= "{{"+s1+"}}"+"{{"+s2+"}}"+"{{"+s3+"}}"+"{{"+s4+"}}"+"{{"+s5+"}}"+"{{"+s6+"}}"+"{{"+s7+"}}"
          #print(df['style'][i])
          sql_update_query = """Update map_data_road_junction set style = %s where id = %s"""
          cursor.execute(sql_update_query, (str(df['style'][i]), str(df['id'][i])))
          connection.commit()
          count = cursor.rowcount
          sql_update_query = """Update map_data_grid_objects set style = %s where type_id = %s AND type=%s"""
          cursor.execute(sql_update_query, (str(df['style'][i]), str(df['id'][i]),'Road Junction'))
          connection.commit()
          count = cursor.rowcount
          print(count, "Record Updated successfully ")

