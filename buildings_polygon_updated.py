#!/usr/bin/env python
# coding: utf-8

# In[1]:


import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
import random
import psycopg2
import re


# In[2]:


connection = psycopg2.connect(user="postgres",
                                  password="Jaya#2707",
                                  host="ikshan.cknt8xlkk69r.us-east-1.rds.amazonaws.com",
                                  port="5432",
                                  database="postgres")
cursor = connection.cursor()

df = pd.read_sql_query("select * from map_data_buildings WHERE grid_zoom9 IS NOT NULL",con=connection)


# In[12]:


df.polygon


# In[4]:


colours = ["[209/255, 194/255, 206/255]",
"[26/255, 252/255, 218/255]",
"[221/255, 211/255, 172/255]",
"[255/255, 223/255, 211/255]",
"[252/255, 250/255, 237/255]",
"[144/255, 212/255, 231/255]",
"[181/255, 230/255, 229/255]",
"[204/255, 218/255, 229/255]",
"[208/255, 229/255, 185/255]",
"[172/255, 197/255, 232/255]"]


# In[6]:


n=len(df)
for i in range(n):
    xa=[]
    ya=[]
    l1=df['polygon'][i]
    #print("l1:",l1)
    l2=re.findall('([0-9]*\.[0-9]*)', l1)
    #if there are more than 1 polygon
    #print("l2:",l2)
    #print(i)
    m=int(len(l2)/2)
    for j in range(m):
        #print("j:",j)
        #print("j1:",j1)
        lx=l2[2*j]
        ly=l2[2*j+1]
        xa.append(float(lx))
        #print(i)
        ya.append(float(ly))
    centroid_x = float(df['grid_centroid_x'][i])
    centroid_y = float(df['grid_centroid_y'][i])
    df['x_max'][i]=max(xa)
    df['x_min'][i]=min(xa)
    df['y_max'][i]=max(ya)
    df['y_min'][i]=min(ya)
    df['x_min_map'][i] = float(min(xa))-centroid_x
    df['x_max_map'][i] = float(max(xa))-centroid_x
    df['y_max_map'][i] = float(max(ya))-centroid_y
    df['y_min_map'][i] = float(min(ya))-centroid_y
    xmin = float(df['x_min_map'][i])*250
    xmax = float(df['x_max_map'][i])*250
    zmax= float(df['y_max_map'][i])*250
    zmin = float(df['y_min_map'][i])*250
    df['x_min_map1'][i] = xmin
    df['x_max_map1'][i] = xmax
    df['y_min_map1'][i] = zmin
    df['y_max_map1'][i] = zmax

    random_index = random.randint(0,9)
    c1 = colours[random_index]
    df['building_colour'][i] = c1
    x_trans = 0
    y_trans = 0
    z_trans = 0
    y=0.04
    scale = 1
    #Difference(
    s0 = "Box( ["+str(xmax)+","+"0"+","+ str(zmin)+"],["+str(xmax)+","+ str(y)+","+ str(zmax)+"],"+"\'hollow\'"+","+"\'scale\',"+"("+str(scale)+","+str(scale)+","+str(scale)+"),"+"Texture( Pigment( \'color\',"+ c1+")),\'translate\',("+str(x_trans)+","+str(y_trans)+","+str(z_trans)+"),"+"Finish( \'ambient\', 0.3,\'diffuse\', 0.6,\'phong\', 0.1),Normal ( \'bumps\', 0.05, \'scale\', 0.001),)"
    #print(s0)
    df['style'][i] = "{{"+s0+"}},"
    print(i)
    #print(df['style'][i])
    sql_update_query = """Update map_data_buildings set style = %s where id = %s"""
    cursor.execute(sql_update_query, (str(df['style'][i]), str(df['id'][i])))
    connection.commit()
    count = cursor.rowcount
    sql_update_query = """Update map_data_grid_objects set style = %s where type_id = %s AND type=%s"""
    cursor.execute(sql_update_query, (str(df['style'][i]), str(df['id'][i]),'Building'))
    connection.commit()
    count = cursor.rowcount
    print(count, "Record Updated successfully ")


